import pygame
from pygame.locals import *
from gl import Renderer, Model
import shaders


width = 960
height = 540

deltaTime = 0.0

pygame.init()
screen = pygame.display.set_mode((width,height), pygame.DOUBLEBUF | pygame.OPENGL )
clock = pygame.time.Clock()

rend = Renderer(screen)
rend.setShaders(shaders.vertex_shader, shaders.fragment_shader)

face = Model('model.obj', 'model.bmp')
nave = Model('nave.obj', 'model.bmp')
model2 = Model('model2.obj', 'model.bmp')
model3 = Model('model3.obj', 'model.bmp')
planeta = Model('planeta.obj', 'model.bmp')

background_image = pygame.image.load("bg.jpeg").convert()

pygame.mixer.music.load("kingdom_come_deliverance_city_music_3(us).mp3")
pygame.mixer.music.play(-1)

rend.scene.append(face)
face.position.z = -5


isRunning = True
while isRunning:

    screen.blit(background_image, [0, 0])
    keys = pygame.key.get_pressed()

    # Traslacion de camara
    if keys[K_d]:
        rend.camPosition.x += 1 * deltaTime
    if keys[K_a]:
        rend.camPosition.x -= 1 * deltaTime
    if keys[K_w]:
        rend.camPosition.z -= 1 * deltaTime
    if keys[K_s]:
        rend.camPosition.z += 1 * deltaTime
    if keys[K_q]:
        rend.camPosition.y -= 1 * deltaTime
    if keys[K_e]:
        rend.camPosition.y += 1 * deltaTime

    if keys[K_LEFT]:
        if rend.valor > 0:
            rend.valor -= 0.1 * deltaTime

    if keys[K_RIGHT]:
        if rend.valor < 0.2:
            rend.valor += 0.1 * deltaTime

    # Rotacion de camara
    if keys[K_z]:
        rend.camRotation.y += 15 * deltaTime
    if keys[K_x]:
        rend.camRotation.y -= 15 * deltaTime


    for ev in pygame.event.get():
        if ev.type == pygame.QUIT:
            isRunning = False

        elif ev.type == pygame.KEYDOWN:
            if ev.key == pygame.K_ESCAPE:
                isRunning = False

            if ev.key == K_1:
                #rend.filledMode()
                rend.setShaders(shaders.vertex_general, shaders.toon_shader)
            if ev.key == K_2:
                #rend.wireframeMode()
                rend.setShaders(shaders.vertex_general, shaders.guate_shader)
            if ev.key == K_3:
                rend.setShaders(shaders.vertex_general, shaders.colombia_shader)
            if ev.key == K_4:
                rend.setShaders(shaders.vertex_general, shaders.lithuania_shader)
            if ev.key == K_5:
                rend.setShaders(shaders.vertex_shader, shaders.fragment_shader)
            if ev.key == K_6:
                rend.scene.clear()
                rend.scene.append(nave)
            if ev.key == K_7:
                rend.scene.clear()
                rend.scene.append(planeta)
            if ev.key == K_8:
                rend.scene.clear()
                rend.scene.append(model3)
            if ev.key == K_9:
                rend.scene.clear()
                rend.scene.append(model2)
            if ev.key == K_0:
                rend.scene.clear()
                rend.scene.append(face)




    rend.tiempo += deltaTime
    deltaTime = clock.tick(60) / 1000

    rend.render()

    pygame.display.flip()

pygame.mixer.music.stop()
pygame.quit()